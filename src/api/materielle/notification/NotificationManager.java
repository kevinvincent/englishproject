package api.materielle.notification;
import java.util.Calendar;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.support.v4.app.NotificationCompat;

import com.example.ok.R;

import api.manage.words.WordsManagerImpl;
import englishNotification.MainActivity;

public class NotificationManager extends BroadcastReceiver {

	private PendingIntent pendingIntent;
	private Calendar calendar = Calendar.getInstance();
	private AlarmManager alarmManager;
	private Context MainActivityContext;
	private long time;
    private int icone;


    @Override
	public void onReceive(Context context, Intent intent)
	{
        android.app.NotificationManager notificationManager = (android.app.NotificationManager)
                context.getSystemService(Context.NOTIFICATION_SERVICE);
        android.app.Notification notification = new NotificationCompat.Builder(context)
                .setContentTitle("new words")
                .setContentText(new WordsManagerImpl(context).dispatch())
                .setSmallIcon(R.drawable.ic_launcher)
                .setContentIntent(PendingIntent
                        .getActivity(context, 0, new Intent(context, MainActivity.class), 0))
                .build();

        notificationManager.notify(0, notification);
	}


    public void display(Context context) {
		setMainActivityContext(context);
		Intent Intent = new Intent(getMainActivityContext(), NotificationManager.class);
		setPendingIntent(PendingIntent.getBroadcast(getMainActivityContext(), 0, Intent, 0));
		setAlarmManager((AlarmManager) getMainActivityContext()
                .getSystemService(Context.ALARM_SERVICE));
		getAlarmManager().setInexactRepeating(AlarmManager.RTC,
                getCalendar().getTimeInMillis(), getTime(), getPendingIntent());
	}

	private PendingIntent getPendingIntent() {
		return pendingIntent;
	}

	private void setPendingIntent(PendingIntent pendingIntent) {
		this.pendingIntent = pendingIntent;
	}

	private Calendar getCalendar() {
		return calendar;
	}



	private AlarmManager getAlarmManager() {
		return alarmManager;
	}

	private void setAlarmManager(AlarmManager alarmManager) {
		this.alarmManager = alarmManager;
	}

	private Context getMainActivityContext() {
		return MainActivityContext;
	}

	private void setMainActivityContext(Context mainActivityContext) {
		MainActivityContext = mainActivityContext;
	}

	private long getTime() {
		return time;
	}

	public void setTime(long time) {
		this.time = time;
	}

    public int getIcone() {
        return icone;
    }

    public void setIcone(int icone) {
        this.icone = icone;
    }

}
