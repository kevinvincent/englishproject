package api.materielle.notification;
import android.content.Context;

public interface Notification {
	/**
	 * Permet d'afficher une notification à l'utilisateur
	 */
	void display(Context context);
	void notificationSetIcone(int icone);
	void notificationSetRepeat(long time);
}