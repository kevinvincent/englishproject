package api.materielle.notification;

import android.content.Context;

public class NotificationImpl implements Notification {
	
	NotificationManager manager;

    public NotificationImpl() {
        manager = new NotificationManager();
    }

	@Override
	public void display(Context context) {
		getManager().display(context);
	}

	@Override
	public void notificationSetIcone(int icone) {
		getManager().setIcone(icone);
	}
	
	@Override
	public void notificationSetRepeat(long time) {
		getManager().setTime(time);
	}
	
	public NotificationManager getManager() {
		return manager;
	}

}
