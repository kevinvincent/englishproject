package api.manage.words;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;

import manage.ressource.Query;
import manage.ressource.QueryValue;

/**
 * Couche intermédiaire du package. récupère les données de la couche basse
 * qui est représenté par DatabaseHandler.
 */
public class wordsDAOImpl implements wordsDAO<WordsDTO> {

    private SQLiteDatabase bdd;
    private DatabaseHandler mHandler;
    private Context context;

    public wordsDAOImpl(Context context){
        setContext(context);
        this.mHandler = new DatabaseHandler(context, DatabaseInfo.NOM, null, DatabaseInfo.VERSION);
    }

    public ArrayList<WordsDTO> getAllElements() {
        open();
        ArrayList<WordsDTO> elements = new ArrayList<>();
        Cursor  cursor = getBdd().rawQuery(QueryValue.getPropValue(getContext(), Query.SELECT_ALL), null);
        if (cursor .moveToFirst()) {

            while (!cursor.isAfterLast()) {
                WordsDTO data = new WordsDTO();
                data.setId(cursor.getInt(DatabaseInfo.NUM_ID));
                data.setWord(cursor.getString(DatabaseInfo.NUM_WORD));
                data.setActive(cursor.getInt(DatabaseInfo.NUM_ACTIVATED));
                elements.add(data);
                cursor.moveToNext();
            }
        }
        return elements;
    }

    @Override
    public void insertWords(WordsDTO row) {
        open();
        insert(row);
        close();
    }

    @Override
    public void deleteWords(int id, WordsDTO row) {
        open();
        delete(id, row);
        close();
    }


    @Override
    public WordsDTO searchWords(String word) {
        open();
        return search(word);
    }

    @Override
    public void updateWord(int id, WordsDTO rowUpdated) {
        open();
        update(id, rowUpdated);
        close();
    }

    private void close() {
        getBdd().close();
    }

    private void open() {
        setBdd(getmHandler().getWritableDatabase());
    }

    private SQLiteDatabase getBdd() {
        return bdd;
    }

    private void setBdd(SQLiteDatabase bdd) {
        this.bdd = bdd;
    }

    private DatabaseHandler getmHandler() {
        return mHandler;
    }

    private Context getContext() {
        return context;
    }

    private void setContext(Context context) {
        this.context = context;
    }

    private void setmHandler(DatabaseHandler mHandler) {
        this.mHandler = mHandler;
    }

    private void insert(WordsDTO row) {
        ContentValues values = new ContentValues();
        values.put(DatabaseInfo.WORD, row.getWord());
        values.put(DatabaseInfo.ACTIVATED,  row.getActive());
        getBdd().insert(DatabaseInfo.TABLE, null, values);

    }

    private WordsDTO search(String word) {
        WordsDTO databasemodel = new WordsDTO();
        Cursor c = getBdd().query(DatabaseInfo.TABLE,
                new String[]{DatabaseInfo.ID, DatabaseInfo.WORD, DatabaseInfo.ACTIVATED},
                DatabaseInfo.WORD + " LIKE ?",
                new String[]{"%" + word + "%"}, null, null, null, null);
        databasemodel.setId(c.getInt(DatabaseInfo.NUM_ID));
        databasemodel.setWord(c.getString(DatabaseInfo.NUM_WORD));
        databasemodel.setActive(c.getInt(DatabaseInfo.NUM_ACTIVATED));
        close();
        return databasemodel;
    }

    private void delete(int id, WordsDTO row) {
        getBdd().delete(DatabaseInfo.TABLE, DatabaseInfo.ID + "=" + id, null);
    }

    private void update(int id, WordsDTO rowUpdated) {
        ContentValues values = new ContentValues();
        values.put(DatabaseInfo.WORD, rowUpdated.getWord());
        values.put(DatabaseInfo.ACTIVATED,  rowUpdated.getActive());
        getBdd().update(DatabaseInfo.TABLE, values, DatabaseInfo.ID + "=" + id, null);
    }
}
