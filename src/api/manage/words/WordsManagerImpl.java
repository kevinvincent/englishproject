package api.manage.words;

import android.content.Context;
import android.util.Log;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.Random;

import manage.ressource.FileInfo;

/**
 * Couche suppérieur du package
 */
public class WordsManagerImpl implements WordsManager{
	private ArrayList<WordsDTO> wordsToNotify = new ArrayList<WordsDTO>();
    private ArrayList<String> listwords = new ArrayList<String>();
    private wordsDAO<WordsDTO> database;
    private Context context;

    public WordsManagerImpl(Context context) {
        this.context = context;
    }

    @Override
	public String dispatch() {
        ArrayList<String> wordsToSend = read();
        int random = (new Random()).nextInt(wordsToSend.size()-1);
		return wordsToSend.get(random);
	}

	@Override
	public void add(String word) {
        Init();
        WordsDTO data = new WordsDTO();
        data.setWord(word);
        data.setActive(DatabaseInfo.ACTIV);
        getDatabase().insertWords(data);
        update();
	}



    @Override
	public void remove(String words) {
        Init();
        WordsDTO data = getDatabase().searchWords(words);
        data.setActive(DatabaseInfo.DESACTIVE);
        getDatabase().updateWord(data.getId(), data);
        update();
    }

    /**
     * Initialise la connextion à la base
     */
	private void Init() {
        setDatabase(new wordsDAOImpl(getContext()));
	}

    private void writeWordsInTextFile(){
        ArrayList<WordsDTO> elements = getDatabase().getAllElements();
        for (WordsDTO words : elements) {
            if (words.getActive() == DatabaseInfo.ACTIV) {
                getWordsToNotify().add(words);
            }
        }
        insert(getWordsToNotify());
    }


    @Override
    public ArrayList<String> read() {
		return readFromFile();
	}

	private ArrayList<WordsDTO> getWordsToNotify() {
		return wordsToNotify;
	}

    private wordsDAO<WordsDTO> getDatabase() {
        return database;
    }

    private Context getContext() {
        return context;
    }

    private void setWordsToNotify(ArrayList<WordsDTO> wordsToNotify) {
        this.wordsToNotify = wordsToNotify;
    }

    private void setContext(Context context) {
        this.context = context;
    }

    private void setDatabase(wordsDAO<WordsDTO> database) {
        this.database = database;
    }

    private void insert(ArrayList<WordsDTO> wordsToNotify) {
            writeToFile(wordsToNotify);
    }

    private ArrayList<String> getListwords() {
        return listwords;
    }

    private void setListwords(ArrayList<String> listwords) {
        this.listwords = listwords;
    }

    private void writeToFile(ArrayList<WordsDTO> wordsToNotify) {

        File file = new File(context.getFilesDir(), FileInfo.name);
        try {
            file.createNewFile();
            FileOutputStream outputStream;
            try {
                outputStream = getContext().openFileOutput(FileInfo.name, Context.MODE_PRIVATE);
                for (WordsDTO data : wordsToNotify) {
                    outputStream.write((data.getWord()+"\n").getBytes());
                }
                outputStream.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        wordsToNotify.clear();
    }


    private ArrayList<String> readFromFile() {

        ArrayList<String> receiveString = new ArrayList();

        try {
            InputStream inputStream = getContext().openFileInput(FileInfo.name);

            if ( inputStream != null ) {
                InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
                BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
                String line = "";

                while ( (line = bufferedReader.readLine()) != null ) {
                    receiveString.add(line);
                }
                inputStream.close();
            }
        }
        catch (FileNotFoundException e) {
            Log.e("login activity", "FileInfo not found: " + e.toString());
        } catch (IOException e) {
            Log.e("login activity", "Can not read file: " + e.toString());
        }

        return receiveString;
    }

    /**
     * A voir si à mettre dans un thread
     */
    private void update() {
        writeWordsInTextFile();
    }


}
