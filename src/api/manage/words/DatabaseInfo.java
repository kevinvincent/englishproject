package api.manage.words;

/**
 * Classe statique non instanciable car possède les informations de la
 * base de données.
 */
public abstract  class DatabaseInfo {
    public final static int VERSION = 3;
    public final static String NOM = "Words.db";
    public final static String TABLE = "WordsToLearn";
    public final static String ID = "ID";
    public final static String WORD = "WORD";
    public final static String ACTIVATED = "ACTIVATED";
    public final static int NUM_ID = 0;
    public final static int NUM_WORD = 1;
    public final static int NUM_ACTIVATED = 2;
    public final static int ACTIV = 1;
    public final static int DESACTIVE = 0;
}
