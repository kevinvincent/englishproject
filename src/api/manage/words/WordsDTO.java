package api.manage.words;

/**
 * Modèle de la base de données.
 */
public class WordsDTO {
    private int id;
    private String word;
    private int Active;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getActive() {
        return Active;
    }

    public void setActive(int active) {
        Active = active;
    }

    public String getWord() {
        return word;
    }

    public void setWord(String word) {
        this.word = word;
    }
}
