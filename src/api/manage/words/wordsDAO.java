package api.manage.words;

import android.database.Cursor;

import java.util.ArrayList;

/**
 * Interagit avec la base de données en respectant le modèle DAO
 * @param <E>
 *     Le paramètre permettra d'utiliser cette interface sur n'importe quelle type d'objet.
 */
public interface wordsDAO<E> {
	public ArrayList<E> getAllElements();
	public void insertWords(E row);
	public void deleteWords(int id, E row);
	public E searchWords(String word);
    public void updateWord(int id, E rowUpdated);
}
