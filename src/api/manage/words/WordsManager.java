package api.manage.words;

import java.util.ArrayList;

/** Cette interface permettra
 * de restreindre  WordsDAO qui sera 
 * chargé en données. L'opération de lecture de données
 * devra se faire lorsque nécessaire soit pendant une mise à jour de la liste.
 * */
public interface WordsManager {
	/**
	 * renvoie la liste des mots actives
	 */
	public ArrayList<String> read();
	/**
	 * ajoute un mot
	 * @param words
	 * le mot que l'on souhaite ajouté
	 */
	public void add(String words);

	/**
	 * retire un mot de la liste
	 * @param words
	 * le mot que l'on souhaite retirer
	 */
	public void remove(String words);

	/**
	 * fourni le mot choisi dans le fichier texte
	 * @return
	 * mot choisi
	 */
	public String dispatch();

}
