package api.manage.words;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.database.sqlite.SQLiteDatabase.CursorFactory;
import manage.ressource.Query;
import manage.ressource.QueryValue;

/**
 * Couche basse du package.
 * Crétion de la table pour pouvoir stocker les données.
 */
public class DatabaseHandler extends SQLiteOpenHelper {

    Context context;
    public DatabaseHandler(Context context, String nom, CursorFactory factory, int version) {
        super(context, nom, factory, version);
        setContext(context);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(QueryValue.getPropValue(getContext(), Query.CREATE_TABLE));
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int i, int i1) {
        db.execSQL(QueryValue.getPropValue(getContext(), Query.DROP_TABLE));
        onCreate(db);
    }

    public Context getContext() {
        return context;
    }

    public void setContext(Context context) {
        this.context = context;
    }
}
