package manage.ressource;

import android.content.Context;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

/**
 * Created by vincent on 25/07/15.
 */
public class QueryValue {



    public static String getPropValue(Context context, Query query) {
        String result = "";
        InputStream inputStream;
        try {
            Properties prop = new Properties();
            String propFileName = "Query.properties";

            inputStream = context.getAssets().open("Query.properties");

            if (inputStream != null) {
                prop.load(inputStream);
            } else {
                throw new FileNotFoundException("property file '" + propFileName + "' not found in the classpath");
            }
            result = prop.getProperty(String.valueOf(query));
            inputStream.close();
        } catch (Exception e) {
        }
        return result;
    }
}
