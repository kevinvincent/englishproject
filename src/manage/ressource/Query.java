package manage.ressource;

/**
 * Created by vincent on 25/07/15.
 */
public enum Query {
    CREATE_TABLE,
    DROP_TABLE,
    SELECT_ALL
}
