package englishNotification;
import com.example.ok.R;
import android.app.Activity;
import android.os.Bundle;

import java.util.ArrayList;

import api.materielle.notification.Notification;
import api.materielle.notification.NotificationImpl;
import command.CommandManager;
import command.InsertCommand;
import command.SeeWordsCommand;

public class MainActivity extends Activity
{
    private ArrayList<CommandManager> commandList;

    @Override
    public void onCreate(Bundle savedInstanceState)
    {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Notification notification = new NotificationImpl();
		notification.notificationSetRepeat(10000);
		notification.display(getApplicationContext());
        ExecuteCommands();
    }

    private void ExecuteCommands() {
        commandList = new ArrayList<>();
        commandList.add(new InsertCommand(R.id.insertCommand, this));
        commandList.add(new SeeWordsCommand(R.id.seeWordsCommand, this));
        for (CommandManager command : commandList) {
            command.execute();
        }
    }

}