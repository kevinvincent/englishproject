package command;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.ok.R;

/**
 * Created by vincent on 15/08/15.
 */
public class InsertCommand extends CommandManagerImpl {
    EditText editText;

    public InsertCommand (int id, Activity activity) {
        super(id,activity);
        this.editText = new EditText(activity);
    }

    @Override
    public void execute() {
        super.execute();
        setEditText((EditText) getActivity().findViewById(R.id.editText));
    }

    private EditText getEditText() {
        return editText;
    }

    private void setEditText(EditText editText) {
        this.editText = editText;
    }

    @Override
    public void onClick(View view) {
        Context context = getActivity().getApplicationContext();
        getWordsManager().add(getEditText().getText().toString());
        CharSequence text = "Le mot " + getEditText().getText().toString() + " a été inséré";
        int duration = Toast.LENGTH_SHORT;
        Toast toast = Toast.makeText(context, text, duration);
        toast.show();
    }
}


