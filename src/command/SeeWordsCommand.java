package command;

import android.app.Activity;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.example.ok.R;

import java.util.ArrayList;

/**
 * Created by vincent on 16/08/15.
 */
public class SeeWordsCommand extends CommandManagerImpl{
    private TextView textView;
    private Button button;

    public SeeWordsCommand(int id, Activity activity) {
        super(id, activity);
        this.textView = new TextView(activity);
    }

    @Override
    public void execute() {
        super.execute();
        setTextView((TextView) getActivity().findViewById(R.id.textView));
    }

    @Override
    public void onClick(View view) {
        ArrayList<String> listString = getWordsManager().read();
        getTextView().setText(listString.toString() +" "+listString.size());
    }

    private TextView getTextView() {
        return textView;
    }

    private void setTextView(TextView textView) {
        this.textView = textView;
    }

    public Button getButton() {
        return button;
    }

    public void setButton(Button button) {
        this.button = button;
    }
}
