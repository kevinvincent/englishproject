package command;

import android.view.View;

/**
 * Created by vincent on 15/08/15.
 */
public interface CommandManager extends View.OnClickListener{
    public void execute();
}
