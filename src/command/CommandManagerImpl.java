package command;

import android.app.Activity;
import android.widget.Button;

import api.manage.words.WordsManager;
import api.manage.words.WordsManagerImpl;

/**
 * Created by vincent on 16/08/15.
 */
public abstract class CommandManagerImpl implements CommandManager {

    private WordsManager wordsManager;
    private Button button;
    private Activity activity;
    private int id;

    public CommandManagerImpl(int id, Activity activity) {
        this.activity = activity;
        this.button = new Button(activity);
        this.id = id;
        setWordsManager(new WordsManagerImpl(activity.getApplicationContext()));
    }
    @Override
    public void execute() {
        setButton((Button) getActivity().findViewById(getId()));
        getButton().setOnClickListener(this);
    }

    protected WordsManager getWordsManager() {
        return wordsManager;
    }

    protected void setWordsManager(WordsManager wordsManager) {
        this.wordsManager = wordsManager;
    }

    protected Activity getActivity() {
        return activity;
    }

    protected void setActivity(Activity activity) {
        this.activity = activity;
    }

    protected int getId() {
        return id;
    }

    protected void setId(int id) {
        this.id = id;
    }

    protected Button getButton() {
        return button;
    }

    protected void setButton(Button button) {
        this.button = button;
    }
}
